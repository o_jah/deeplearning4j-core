/*
 *
 *  * Copyright 2015 Skymind,Inc.
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package org.deeplearning4j.nn.conf;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.deeplearning4j.nn.conf.override.ConfOverride;
import org.nd4j.linalg.factory.Nd4j;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Configuration for a multi layer network
 *
 * @author Adam Gibson
 */

public class MultiLayerConfiguration implements Serializable, Cloneable {

	protected List<NeuralNetConfiguration> confs;
	protected boolean pretrain = true;
	@Deprecated
	protected double dampingFactor = 100;
	protected Map<Integer, InputPreProcessor> inputPreProcessors = new HashMap<Integer, InputPreProcessor>();
	protected boolean backprop = false;
	// whether to redistribute params or not
	protected boolean redistributeParams = false;

	public List<NeuralNetConfiguration> getConfs() {
		return confs;
	}

	public void setConfs(List<NeuralNetConfiguration> confs) {
		this.confs = confs;
	}

	public boolean isPretrain() {
		return pretrain;
	}

	public void setPretrain(boolean pretrain) {
		this.pretrain = pretrain;
	}

	public double getDampingFactor() {
		return dampingFactor;
	}

	public void setDampingFactor(double dampingFactor) {
		this.dampingFactor = dampingFactor;
	}

	public Map<Integer, InputPreProcessor> getInputPreProcessors() {
		return inputPreProcessors;
	}

	public void setInputPreProcessors(
			Map<Integer, InputPreProcessor> inputPreProcessors) {
		this.inputPreProcessors = inputPreProcessors;
	}

	public boolean isBackprop() {
		return backprop;
	}

	public void setBackprop(boolean backprop) {
		this.backprop = backprop;
	}

	public boolean isRedistributeParams() {
		return redistributeParams;
	}

	public void setRedistributeParams(boolean redistributeParams) {
		this.redistributeParams = redistributeParams;
	}

	public MultiLayerConfiguration() {
	}

	public MultiLayerConfiguration(List<NeuralNetConfiguration> confs,
			boolean pretrain, double dampingFactor,
			Map<Integer, InputPreProcessor> inputPreProcessors,
			boolean backprop, boolean redistributeParams) {
		super();
		this.confs = confs;
		this.pretrain = pretrain;
		this.dampingFactor = dampingFactor;
		this.inputPreProcessors = inputPreProcessors;
		this.backprop = backprop;
		this.redistributeParams = redistributeParams;
	}

	/**
	 *
	 * @return JSON representation of NN configuration
	 */
	public String toYaml() {
		ObjectMapper mapper = NeuralNetConfiguration.mapperYaml();
		try {
			return mapper.writeValueAsString(this);
		} catch (com.fasterxml.jackson.core.JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Create a neural net configuration from json
	 * 
	 * @param json
	 *            the neural net configuration from json
	 * @return {@link org.deeplearning4j.nn.conf.MultiLayerConfiguration}
	 */
	public static MultiLayerConfiguration fromYaml(String json) {
		ObjectMapper mapper = NeuralNetConfiguration.mapperYaml();
		try {
			return mapper.readValue(json, MultiLayerConfiguration.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 *
	 * @return JSON representation of NN configuration
	 */
	public String toJson() {
		ObjectMapper mapper = NeuralNetConfiguration.mapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (com.fasterxml.jackson.core.JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Create a neural net configuration from json
	 * 
	 * @param json
	 *            the neural net configuration from json
	 * @return {@link org.deeplearning4j.nn.conf.MultiLayerConfiguration}
	 */
	public static MultiLayerConfiguration fromJson(String json) {
		ObjectMapper mapper = NeuralNetConfiguration.mapper();
		try {
			return mapper.readValue(json, MultiLayerConfiguration.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String toString() {
		return toJson();
	}

	public NeuralNetConfiguration getConf(int i) {
		return confs.get(i);
	}

	@Override
	public MultiLayerConfiguration clone() {
		try {
			MultiLayerConfiguration clone = (MultiLayerConfiguration) super.clone();

			if (clone.confs != null) {
				List<NeuralNetConfiguration> list = new ArrayList<NeuralNetConfiguration>();
				for (NeuralNetConfiguration conf : clone.confs) {
					list.add(conf.clone());
				}
				clone.confs = list;
			}

			if (clone.inputPreProcessors != null) {
				Map<Integer, InputPreProcessor> map = new HashMap<Integer, InputPreProcessor>();
				for (Map.Entry<Integer, InputPreProcessor> entry : clone.inputPreProcessors
						.entrySet()) {
					map.put(entry.getKey(), entry.getValue().clone());
				}
				clone.inputPreProcessors = map;
			}

			return clone;

		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	public InputPreProcessor getInputPreProcess(int curr) {
		return inputPreProcessors.get(curr);
	}


	public static class Builder {

		protected List<NeuralNetConfiguration> confs = new ArrayList<NeuralNetConfiguration>();
		protected boolean pretrain = true;
		protected double dampingFactor = 100;
		protected Map<Integer, InputPreProcessor> inputPreProcessors = new HashMap<Integer, InputPreProcessor>();
		protected boolean backprop = false;
		protected boolean redistributeParams = false;
		@Deprecated
		protected Map<Integer, ConfOverride> confOverrides = new HashMap<Integer, ConfOverride>();

		public List<NeuralNetConfiguration> getConfs() {
			return confs;
		}

		public void setConfs(List<NeuralNetConfiguration> confs) {
			this.confs = confs;
		}

		public boolean isPretrain() {
			return pretrain;
		}

		public void setPretrain(boolean pretrain) {
			this.pretrain = pretrain;
		}

		public double getDampingFactor() {
			return dampingFactor;
		}

		public void setDampingFactor(double dampingFactor) {
			this.dampingFactor = dampingFactor;
		}

		public Map<Integer, InputPreProcessor> getInputPreProcessors() {
			return inputPreProcessors;
		}

		public void setInputPreProcessors(
				Map<Integer, InputPreProcessor> inputPreProcessors) {
			this.inputPreProcessors = inputPreProcessors;
		}

		public boolean isBackprop() {
			return backprop;
		}

		public void setBackprop(boolean backprop) {
			this.backprop = backprop;
		}

		public boolean isRedistributeParams() {
			return redistributeParams;
		}

		public void setRedistributeParams(boolean redistributeParams) {
			this.redistributeParams = redistributeParams;
		}

		public Map<Integer, ConfOverride> getConfOverrides() {
			return confOverrides;
		}

		public void setConfOverrides(Map<Integer, ConfOverride> confOverrides) {
			this.confOverrides = confOverrides;
		}

		/**
		 * Whether to redistribute parameters as a view or not
		 * 
		 * @param redistributeParams
		 *            whether to redistribute parameters as a view or not
		 * @return
		 */
		public Builder redistributeParams(boolean redistributeParams) {
			this.redistributeParams = redistributeParams;
			return this;
		}

		/**
		 * Specify the processors. These are used at each layer for doing things
		 * like normalization and shaping of input.
		 * 
		 * @param processor
		 *            what to use to preProcess the data.
		 * @return builder pattern
		 */
		public Builder inputPreProcessor(Integer layer,
				InputPreProcessor processor) {
			inputPreProcessors.put(layer, processor);
			return this;
		}

		public Builder inputPreProcessors(
				Map<Integer, InputPreProcessor> processors) {
			this.inputPreProcessors = processors;
			return this;
		}

		/**
		 * Whether to do back prop or not
		 * 
		 * @param backprop
		 *            whether to do back prop or not
		 * @return
		 */
		public Builder backprop(boolean backprop) {
			this.backprop = backprop;
			return this;
		}

		@Deprecated
		public Builder dampingFactor(double dampingFactor) {
			this.dampingFactor = dampingFactor;
			return this;
		}

		/**
		 * Whether to do pre train or not
		 * 
		 * @param pretrain
		 *            whether to do pre train or not
		 * @return builder pattern
		 */
		public Builder pretrain(boolean pretrain) {
			this.pretrain = pretrain;
			return this;
		}

		public Builder confs(List<NeuralNetConfiguration> confs) {
			this.confs = confs;
			return this;

		}

		public MultiLayerConfiguration build() {
			MultiLayerConfiguration conf = new MultiLayerConfiguration();
			conf.confs = this.confs;
			conf.pretrain = pretrain;
			conf.dampingFactor = dampingFactor;
			conf.backprop = backprop;
			conf.inputPreProcessors = inputPreProcessors;
			conf.redistributeParams = redistributeParams;
			Nd4j.getRandom().setSeed(conf.getConf(0).getSeed());
			return conf;

		}

	}
}
